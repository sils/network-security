# Encryption Algorithms

# Enipherment Mechanisms

A.k.a. encryption algorithms.

Some may also be used for integrity services.

## "Old Paradigm"

![AandB](intruder.png)

The intruder can of course be a law enforcement agency, but then it's legal:

![law](lea.png)

## Trusted Third Party

E.g. A and B don't trust each other (customer/merchant) and both related
to a trusted third party that settles disputes.

![Trusted Third Party](ttp.png)

## Terminology

- **Encryption**: `x` is converted into a ciphertext with a key `K`. We write
  `eK(x)`.
- **Decryption**: `dK(y)`
- Symmetric vs. Asymmetric!

# Block vs. Stream

- **Block**: Encrypt a sequence block wise without changing a key.
    - Security relies on *encryption function*.
- **Stream**: Encrypt sequences of "short" data (typical: one bit or byte)
  under a *changing key stream*.
    - Security relies on *stream generator*.

## Block Cipher

- Permutate the blocks, i.e. a reversible function!
- Efficiently computable.

- Observing one ciphertext block may not increase the information about the
  corresponding plain text block.

![Chained Block](block_chain.png)


### Feistel Ciphers

Concatenate result XORred one by one block by block.

![Chained Block](blockchain.png)

![Feistel](feistel.png)

### DES

**Data Encryption Standard**.

Uses Feistel Cipher.

#### Triple DES

Data -> DES Encrypt (Key 1) -> DES Decrypt (Key 2) -> DES Encrypt (Key 3)

### AES

See <http://www.formaestudio.com/rijndaelinspector/archivos/rijndaelanimation.html>

Try it out: <http://www.formaestudio.com/rijndaelinspector/archivos/inspector.html>

**Cipher Block Chaining Mode** vs. **Output Feedback Mode** vs. **Cipher Feedback Mode**

## Stream Ciphers

TODO
