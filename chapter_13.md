# Formal Analysis

Formal Protocol Verification

> Testing shows the presence,
> not the absence of bugs.

*Edgar W. Dijkstra*

# Protocol Specification

The specification is made for **implementors** and **analysts**.
The former need a spec without *meaning*, the latter don't care about
technical details and want to analyze the security aspects.

Alice-and-Bob notation: Both have `send` and `receive` statements. Sending
messages is atomic.

## Cryptography

**Computational Model**: A computationally bounded attackers chance to
distinguish between a secret and a random value is negligible.

Algorithmic correctness: `d_K (e_K (m)) = m`

## Security Proofs

- Use **term rewriting** to check wether an attacker can compute a result
  (usually a secret) from messages from various protocol runs.
- In the computational model, use **reduction proofs** to show
  that an attacker with a non-negligible chance to break the
  protocol would have a non-negligible chance to solve a
  supposedly difficult problem.
    - Difficult problems: factorization, DLP, D-H assumptions
    - Boutique problems: assumptions specific to the protocol
      under analysis; proofs may become trivial tautologies
- Logic derivations of desired properties in a specific security logic

# Public Key Needham-Schroeder Protocol

![Needham](needham.png)

# Dolev-Yao

> **Notation**
>
> ![Notation](notation.png)
>
> States that **A**lice sending a message to **B**ob consisting of a
> plaintext X encrypted under a shared key `K_A,B`.

**Assumption**: Attacker *observes* traffic and can *engage* in protocol runs.

**(Attacker's) Goal**: Obtain the secret that was encrypted!

Analysis is abstract, independent of any specific feature of a crypto
algorithm. Protocol specifications should leave the choice of the actual
crypto algorthms open.

## Howto:

- **Protocol** is specified as a **sequence of messages**.
- **Messages** are lists of **terms** which are **constants** of of the
  form `e_k(t)`, i.e. an encryption of another term.
- All parties (sender, receiver, attacker) can encrypt and decrypt with their
  available keys.
- No guessing/social engineering.

$L$: {$e_{K_i} , d_{K_i} | K_i$ encryption key of party $i$}

If epsilon, the empty word, is in $L$, it's apparently insecure. See slide 12.

$w \rightarrow e_{K_b} w, w \rightarrow e_{K_a} d_{K_b} w$

Variation of the previous protocol:

- $A \rightarrow B : A, e_{K_b} (e_{K_b} (m), A), B$
- $B \rightarrow A : B, e_{K_a} (e_{K_a} (m), B), A$

But apparently Eve can break it somehow by applying her own private key to
the already encrypted thing! Poor Bob!

> “Analysis in the Dolev-Yao model” is sometimes shorthand
> for analysis in the presence of an active attacker that has
> full control of communications.

# BAN Logic

> **Nonces**
>
> Are random or pseudorandom numbers that can be used for encryption to
> prevent replay attacks.

Design a *logic* for reasoning about *beliefs*.

![Beliefs](bliefs.png)

![Secrets](secrets.png)

## Application:

1. Translate it into a logical specification.

   ![Idealization](idealization.png)

   The idealization is ambiguous and has to capture initial beliefs!

   Watch this:

   ![PKNS](pkns.png)
2. Apply axioms (5 axioms available, see slide 23 ff.)

**Different idealizations may lead to different conclusions!**

![Exercise](excercise.png)

# Security Analysis with Model Checking

