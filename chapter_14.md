# TOR

# Things People Know About You

- IP Adress can be linked to you
- Cookies, ..., browser fingerprinting
- Activities can be used to identify you
- Wireless traffic is easily intercepted

# Mixes

We have *intermediate nodes* between sender and receiver. All parties have
public/private key pairs. To send a document `d`:

- Sender encrypts `d`: $c = e P K_m(r, e P K_r(d))$
- Send that to mix
- Mix decrypts cyphertext, get's $r, e P K_r(d)$, sends it to `r` after
  random delay.

## Attack

Correlate incoming/outgoing traffic at mix

- Document size (send only equally sized docs)
- Time (Random delays, sufficient volume needed, potentially upper delay bound
  exists)
- Compromise a mix (make a network of mixes, lots of encryption layers
  with a link to the next node each)

![The Onion](onion.png)

If a private key is compromised we don't want *future and past* communications
to be compromised. -> Negotiate a new key pare with each relay.

## Censorship

Tor relays might be censored by countries. Tor bridges are not publically
known and not blocked, you have to know them though.

Some censorship happens through deep packet inspection. You may want
obfuscation.

## Deanynomization

Replay, modify, delete cells so protocol specific errors are raised which can
be used to identify the receiver.

Or insert JS code with a ping, measure round trip time?

## DoS Attacks

Force them to use adversary nodes.

Consumes memory at relay using valid protocol
messages, exploiting Tor’s end-to-end reliable data
transport. Could disable the top 20 exit relays in 29 minutes, reducing
Tor’s bandwidth capacity by 35 percent.
